//
//  animationViewController.swift
//  Labb2
//
//  Created by Oskar Eek on 2019-11-07.
//  Copyright © 2019 Oskar Eek. All rights reserved.
//

import UIKit

class animationViewController: UIViewController {

    @IBOutlet weak var animationText: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        startAnimation()
        // Do any additional setup after loading the view.
    }
    
    func startAnimation() {
        animationText.textColor = #colorLiteral(red: 1, green: 0, blue: 0.06772173196, alpha: 1)
        
        
        
        let pulseAnimation = CABasicAnimation(keyPath: "transform.scale")
        pulseAnimation.duration = 3
        pulseAnimation.fromValue = 0.6
        pulseAnimation.toValue = 1
        pulseAnimation.autoreverses = true
        pulseAnimation.repeatCount = Float.greatestFiniteMagnitude
        self.animationText.layer.add(pulseAnimation, forKey: nil)

    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
