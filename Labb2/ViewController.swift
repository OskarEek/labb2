//
//  ViewController.swift
//  Labb2
//
//  Created by Oskar Eek on 2019-10-29.
//  Copyright © 2019 Oskar Eek. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func experianceButton(_ sender: Any) {
        performSegue(withIdentifier: "tableView", sender: self)
    }
    
    @IBAction func skillsButton(_ sender: Any) {
        performSegue(withIdentifier: "animationView", sender: self)
    }
}

