//
//  TableViewController.swift
//  Labb2
//
//  Created by Oskar Eek on 2019-10-29.
//  Copyright © 2019 Oskar Eek. All rights reserved.
//

import UIKit

class TableViewController: UITableViewController {
    
    var works: [Work] = []
    var educations: [Education] = []


    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        for i in 1..<4 {
            let workPictures = ["mic", "folder", "doc"]
            let work = Work(pictureName: "\(workPictures[(i - 1)])", name: "Work \(i)", information: "This is the \(i) work", startAndFinishYear: "2017 - 2019")
            
            works.append(work)
        }
        
        for i in 1..<3 {
            let educationPictures = ["pencil", "book"]
            let education = Education(pictureName: "\(educationPictures[(i - 1)])", name: "Education \(i)", information: "This is the \(i) education", startAndFinishYear:"2014 - 2017")
            
            educations.append(education)
        }
        tableView.reloadData()
        

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 2
    }
     
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if section == 0 {
            return works.count
        }
        else {
            return educations.count
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "workCell",
                                                    for: indexPath) as? TableViewCell {
            if (indexPath.section == 0) {
                let work = works[indexPath.row]
                cell.coverImageView.image = UIImage(systemName: "\(work.pictureName)")
                cell.nameLabel.text = work.name
                cell.startAndFinishLabel.text = "\(work.startAndFinishYear)"
                return cell
            }
            else {
                let education = educations[indexPath.row]
                cell.coverImageView.image = UIImage(systemName: "\(education.pictureName)")
                cell.nameLabel.text = education.name
                cell.startAndFinishLabel.text = "\(education.startAndFinishYear)"
                return cell
            }
            
        }
        return UITableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let sections = ["Work", "Education"]
        return "\(sections[section])"
    }
    
    
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "cellClick", sender: indexPath)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? cellViewController, let indexPath = sender as? IndexPath {
            if indexPath.section == 0 {
                let clickedWork = works[indexPath.row]
                destination.name = clickedWork.name
                destination.text = clickedWork.information
                destination.date  = clickedWork.startAndFinishYear
                destination.imageName = clickedWork.pictureName
            }
            else {
                let clickedEducation = educations[indexPath.row]
                destination.name = clickedEducation.name
                destination.text = clickedEducation.information
                destination.date = clickedEducation.startAndFinishYear
                destination.imageName = clickedEducation.pictureName
            }
            
        }
    }
    
    
    
    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> TableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "workCell", for: indexPath) as? TableViewCell {
            let work = works[indexPath.row]
            cell.nameLabel.text = "\(work.name)"
            cell.startAndFinishLabel.text = "\(work.startAndFinishYear)"
            return cell
        }
        return TableViewCell()
    }
    */
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
