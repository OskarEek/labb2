//
//  cellViewController.swift
//  Labb2
//
//  Created by Oskar Eek on 2019-11-05.
//  Copyright © 2019 Oskar Eek. All rights reserved.
//

import UIKit

class cellViewController: UIViewController {

    var name: String!
    var text: String!
    var date: String!
    var imageName: String!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var dateLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nameLabel.text = name
        textView.text = text
        dateLabel.text = date
        image.image = UIImage(systemName: imageName)
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
