//
//  model.swift
//  Labb2
//
//  Created by Oskar Eek on 2019-11-05.
//  Copyright © 2019 Oskar Eek. All rights reserved.
//

import Foundation

struct Work {
    let pictureName: String
    let name: String
    let information: String
    let startAndFinishYear: String
}

struct Education {
    let pictureName: String
    let name: String
    let information: String
    let startAndFinishYear: String
}
